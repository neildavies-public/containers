#!/bin/sh
set -eu

echo "Sleeping for ${1} seconds ..."

sleep ${1}